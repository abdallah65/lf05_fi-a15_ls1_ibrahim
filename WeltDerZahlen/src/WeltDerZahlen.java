public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  9;
    
    // Anzahl der Sterne in unserer Milchstra�e
    double anzahlSterne = 400000000000.00;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      short alterTage = 7293;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm =  150000; 
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
       int flaecheGroessteLand = 17100000;
    		   
    		   
    // Wie gro� ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println(" Die Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Die Anzahl der Sterne in unserer Milchstra�e: " + anzahlSterne);
    System.out.println("Die Anzahl der Einwohner in Berlin betr�gt: " + bewohnerBerlin);
    System.out.println("Mein alter in Tagen: " + alterTage);
    System.out.println("Das schwerste Tier der Welt ist " + gewichtKilogramm + " schwer." );
    System.out.println("Die gr��te Landfl�che der Erde betr�gt " + flaecheGroessteLand);
    System.out.println("Das kleinste Land der Erde hat eine Fl�che von " + flaecheKleinsteLand +" Quadratkilometer.\n");
 
    
    System.out.printf("%-5s = %-20s = %4d\n", "0!", "", 1);
    System.out.printf("%-5s = %-20s = %4d\n", "1!", "1 * 1", 1);
    System.out.printf("%-5s = %-20s = %4d\n", "2!", "1 * 2", 2);
    System.out.printf("%-5s = %-20s = %4d\n", "3!", "1 * 2 * 3",1 * 2);
    System.out.printf("%-5s = %-20s = %4d\n", "4!", "1 * 2 * 3 * 4",1 * 2 * 3 * 4);
    System.out.printf("%-5s = %-20s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5",1 * 2 * 3 * 4 * 5);
    
    System.out.println("");
    System.out.printf("%-5s%5s%-5s%12s\n","Fahrenheit","|","","Celsius");
    System.out.println("-----------------------------------");
    
    System.out.printf("%-5s%10s%-10s%5s\n",-20,"|","",-28.89);
    System.out.printf("%-5s%10s%-10s%5s\n",-10,"|","",-23.33);
    System.out.printf("%-5s%10s%-10s%5s\n","+0","|","",-17.73);
    System.out.printf("%-5s%10s%-10s%5s\n","+20","|" ,"",-6.67);
    System.out.printf("%-5s%10s%-10s%5s\n","+30","|" ,"",-1.11);
    
  }
}
