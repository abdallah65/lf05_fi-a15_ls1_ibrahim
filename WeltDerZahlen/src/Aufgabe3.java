
public class Aufgabe3 {
	public static void main(String[] args) {
		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));
	}
		
	public static void ausgabe(int a,String b) {
		System.out.print(a+b);
		
	}
	
	public static boolean vergleichen (int a,int b) {
		return a==b;
	}
}