public class Ausgabeformatierung {
    public static void main(String[]args){

        //Aufgabe 1
        String s = "*";
        System.out.printf( "\n%11s", s );
        System.out.printf( "%s\n", s );
        System.out.printf("%8s", s);
        System.out.printf("%7s\n", s);
        System.out.printf("%8s", s);
        System.out.printf("%7s\n", s);
        System.out.printf( "%11s", s );
        System.out.printf( "%s\n", s );

        //Aufgabe 2
        int a = 0;
        int b = 1;
        int c = 2;
        int j = 3;
        int d = 4;
        int e = 5;
        int f = 6;
        int g = 24;
        int h = 120;
        String o = "=";
        System.out.printf( "\n%1d! %3s %19s %3s", a, o, o, b);
        System.out.printf( "\n%1d! %3s %s %17s %3s", b, o, b, o, b);
        System.out.printf( "\n%1d! %3s %s * %s %13s %3s", c, o, b, c, o, c);
        System.out.printf( "\n%1d! %3s %s * %S * %s  %8s %3s", j, o, b, c, j, o, f);
        System.out.printf( "\n%1d! %3s %s * %s * %s * %s  %4s %3s", d, o, b, c, j, d, o, g);
        System.out.printf( "\n%1d! %3s %s * %s * %s * %s * %s %s %1s", e, o, b, c, j, d, e, o, h);
    base();
    }
        public static void base(){
        //Aufgabe 3
        String ab = "\n\n Fahrenheit";
        String bc = "Celsius";
        int a = 20;
        double b = 28.8889;
        int c = 10;
        double d = 23.3333;
        int e = 0;
        double f = 17.7778;
        int g = 20;
        double h = 6.6667;
        int i = 30;
        double j = 1.1111;

        System.out.printf("%-12s| %10s\n", ab,bc);
        System.out.println("------------------------");
        System.out.printf( "%-12d| %10.2f\n" , -a, -b);
        System.out.printf( "%-12d| %10.2f\n" , -c, -d);
        System.out.printf( "%-12d| %10.2f\n" , e, -f);
        System.out.printf( "%-12d| %10.2f\n" , g, -h);
        System.out.printf( "%-12d| %10.2f\n" , i, -j);


        }
}