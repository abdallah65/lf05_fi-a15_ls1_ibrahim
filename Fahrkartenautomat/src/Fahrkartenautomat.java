﻿import java.util.Scanner;

class Fahrkartenautomat {

    public static double fahrkartenbestellungErfassen() {
        int anzahlTickets;
        double ticketPreis;

        Scanner tastatur = new Scanner(System.in);

        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
        System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        System.out.print("Ihre Wahl:");
        ticketPreis = tastatur.nextDouble();

        if(ticketPreis <= 0) {
            System.out.println("Ungueltige Anzahl der Tickets. Preis wurde auf 1 Euro gesetzt.");
            ticketPreis = 1;
        }
        if (ticketPreis == 1) {
            System.out.println("Ihre Wahl: Einzelfahrschein Regeltrarif AB");
            ticketPreis = 2.90;
        }
        if (ticketPreis == 2) {
            System.out.println("Ihre Wahl: Tageskarte Regeltrarif AB");
            ticketPreis = 8.60;
        }
        if (ticketPreis == 3) {
            System.out.println("Ihre Wahl: Kleingruppen-Tageskarte Regeltrarif AB");
            ticketPreis = 23.50;
        }

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        if (anzahlTickets <= 0 || anzahlTickets > 10) {
            System.out.print("Eingabe nicht Valide, Anzahl wird auf 1 gesetzt.\n");
            anzahlTickets = 1;
        }

        else {
            System.out.print("Eingegebene Ticketzahl ist " + anzahlTickets + ". Bitte Warten.");
        }

        return ticketPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format(" Noch zu zahlen: %4.2f %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabebetrag + " wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M?zen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {
        while (true) {

            double zuZahlenderBetrag;
            double rueckgabebetrag;

            zuZahlenderBetrag = fahrkartenbestellungErfassen();
            rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            rueckgeldAusgeben(rueckgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir wuenschen Ihnen eine gute Fahrt.");
            for (int i = 0; i < 8; i++) {
                System.out.print("=");
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            System.out.println("\n\n");
        }
    }
}